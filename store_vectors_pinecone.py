from src.operations import extract_data, text_chunks, download_huggingface_embedding
from langchain.vectorstores import Pinecone as PC
from dotenv import load_dotenv
import os

load_dotenv()

PINECONE_API_KEY = os.getenv("PINECONE_API_KEY")
PINECONE_API_ENV = os.getenv("PINECONE_API_ENV")
HUGGINGFACEHUB_API_TOKEN = os.getenv("HUGGINGFACEHUB_API_TOKEN")

extracted_data = extract_data("data/")

chunk_data = text_chunks(extracted_data)
print(len(chunk_data))

embeddings = download_huggingface_embedding()

index_name = "chatbot"

# query_result = embeddings.embed_query("Hello World")
# print(len(query_result))

# below code needs PINECONE_API_KEY to be set as environment variable 
# Creating Embeddings for Each of The Text Chunks & storing
docsearch = PC.from_texts([t.page_content for t in chunk_data], embeddings, index_name=index_name)
