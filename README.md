# Medical chat bot
#### Note: We are using CPU for this project, not GPU.
# Steps:
* Create Virtual Environment

* Install required libraries
```
pip install -r requirements.txt
```

* Create .env file and add pinecone api and Huggingface Credentials
```
PINECONE_API_KEY = "xxxxxxxxxxxxxxxx"
PINECONE_API_ENV = "xxxxxxxxxxxxxxxx"
```

* Download the quantize model from the link provided in model folder & keep the model in the model directory
```
llama-2-7b-chat.ggmlv3.q4_0.bin
```
* Run store_vectors_pinecone.py once which will store embedding vectors into pinecone vectordb.
```
python store_vectors_pinecone.py
```

* Run app.py
```
python app.py
```

* Open app at localhost
* Ask question

## Libraries used
* Langchain
* Pinecone
* Meta Llama2 model (Downloaded)
* Flask