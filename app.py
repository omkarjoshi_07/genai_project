from flask import Flask, render_template, jsonify, request
from src.operations import extract_data, text_chunks, download_huggingface_embedding
from src.prompt import prompt
from langchain import PromptTemplate
from langchain.chains import RetrievalQA
from langchain.vectorstores import Pinecone as PC
from langchain.prompts import PromptTemplate
from langchain.llms import CTransformers
import os
from dotenv import load_dotenv

app = Flask(__name__)

load_dotenv()

PINECONE_API_KEY = os.getenv("PINECONE_API_KEY")
PINECONE_API_ENV = os.getenv("PINECONE_API_ENV")
HUGGINGFACEHUB_API_TOKEN = os.getenv("HUGGINGFACEHUB_API_TOKEN")

# print(PINECONE_API_ENV)
embeddings = download_huggingface_embedding()

index_name = "chatbot"

# below code needs PINECONE_API_KEY to be set as environment variable 
# As we have already created index in pinecone website, we don't have to use below code.
# docsearch = PC.from_texts([t.page_content for t in chunk_data], embeddings, index_name=index_name)

docsearch = PC.from_existing_index(index_name, embeddings)

# print(docsearch)

ptemplate = PromptTemplate(
    template=prompt,
    input_variables=["context", "question"]
)

llm = CTransformers(
    model="model/llama-2-7b-chat.ggmlv3.q4_0.bin", 
    model_type="llama", 
    config={"max_new_tokens":512, "temperature":0.8}
)

qa = RetrievalQA.from_chain_type(
    llm = llm, 
    chain_type = "stuff", 
    retriever = docsearch.as_retriever(search_kwargs={'k':2}),
    return_source_documents = True,
    chain_type_kwargs = {"prompt": ptemplate}                      
)

# user_input = input(f"Input Prompt: ")
# result = qa({'query': user_input})
# print(result)

@app.route("/")
def index():
    return render_template('chat.html')

@app.route("/get", methods=["GET"])
def chat():
    msg = request.form["msg"]
    user_input = msg
    print(user_input)
    result=qa({"query": user_input})
    print("Response : ", result["result"])
    return str(result["result"])

if __name__ == '__main__':
    app.run(host="localhost", port= 8080, debug= True)