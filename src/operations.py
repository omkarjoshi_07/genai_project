from langchain.embeddings import HuggingFaceEmbeddings
from langchain.document_loaders import PyPDFLoader, PyPDFDirectoryLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter

def extract_data(data):
    """
    Load pdf data from specified directory into documents
    """
    loader = PyPDFDirectoryLoader(data)
    # print(loader)
    documents = loader.load()

    return documents


def text_chunks(extracted_data):
    """
    Split data into chunks
    """
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=500, chunk_overlap=20)
    data = text_splitter.split_documents(extracted_data)
    return data


def download_huggingface_embedding():
    """
    Download embedding model from huggingface
    """
    embeddings = HuggingFaceEmbeddings(model_name="sentence-transformers/all-MiniLM-L6-v2")
    return embeddings

